﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ResourceApi.Controllers
{
   [ApiController]
   [Route("[controller]")]
   public class ResourceController : ControllerBase
   {
      [Authorize]
      [HttpGet]
      public string GetResource()
      {
         return "Secured resource";
      }
   }
}