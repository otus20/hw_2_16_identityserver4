﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;

namespace ConsoleClient
{
   class Program
   {
      static async Task Main(string[] args)
      {
         var client = new HttpClient();

         var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");

         if (disco.IsError)
         {
            Console.WriteLine(disco.Error);
            return;
         }

         var tokenRequest = new ClientCredentialsTokenRequest
         {
            Address = disco.TokenEndpoint,
            ClientId = "consoleClient",
            ClientSecret = "511536EF-F270-4058-80CA-1C89C192F69A",
            Scope = "resourceApi"
         };

         var tokenResponse = await client.RequestClientCredentialsTokenAsync(tokenRequest);

         if (tokenResponse.IsError)
         {
            Console.WriteLine(tokenResponse.Error);
            return;
         }

         Console.WriteLine(tokenResponse.Json);
         Console.Write(Environment.NewLine);
         
         client = new HttpClient();
         client.SetBearerToken(tokenResponse.AccessToken);

         
         var response = await client.GetAsync("https://localhost:6001/resource");

         if (!response.IsSuccessStatusCode)
         {
            Console.WriteLine($"Request resource failed with code: {response.StatusCode}");
         }

         var result =  await response.Content.ReadAsStringAsync();
         Console.WriteLine($"Result: {result}");

         Console.ReadKey();
      }
   }
}